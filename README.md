# Service Usage

* Helmholtz AAI
    * Connected Helmholtz Centres
    * Connected Services
    * Users with active accounts
    * Number of supported VOs

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Old plots

Old plots from Reporting in spring 2021 can still be accessed in branch [`backup-of-plot-data`](https://gitlab.hzdr.de/hifis/overall/communication/service-usage-plots/-/tree/backup-of-plot-data)
